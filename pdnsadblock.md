# pdns adblock with postgresql #

### references ###
https://wiki.hackzine.org/sysadmin/postgresql-change-default-schema.html  
https://doc.powerdns.com/authoritative/backends/generic-postgresql.html#default-schema  
https://people.planetpostgresql.org/devrim/index.php?/archives/80-Installing-and-configuring-PostgreSQL-9.3-and-9.4-on-RHEL-7.html  
https://tableplus.io/blog/2018/04/postgresql-how-to-grant-access-to-users.html

### packages ###
```
yum install -y vim wget epel-release
wget https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
yum install -y pgdg-redhat-repo-latest.noarch.rpm
yum install -y postgresql10-server postgresql10-contrib pdns pdns-backend-postgresql
```
(disable selinux)
```
firewall-cmd --zone=public --add-service=dns --permanent
firewall-cmd --reload
```

### database easy way ###
```
cd /var/lib/pgsql/
wget http://rechentier.ippad.ipp.mpg.de/adblock/pgsql/data.tar.xz
cd data
tar xJf data.tar.xz
systemctl start postgresql-10
systemctl enable postgresql-10
```
`/etc/pdns/pdns.conf:`
```
launch=gpgsql
master=yes
local-address=*IP-Address*
webserver=yes
webserver-allow-from=*Subnet*
webserver-port=8081
```

```
systemctl start pdns
systemctl enable pdns
```

### database from scratch ###
#### create database ####
`/usr/pgsql-10/bin/postgresql-10-setup initdb`

vim /var/lib/pgsql/10/data/pg_hba.conf  
  local trust
```
systemctl start postgresql-10
systemctl enable postgresql-10
```
`psql -U postgres`
```
create user pdns;
alter user pdns createdb;
```
```
psql -U pdns -d postgres
create database pdns;
```
`psql -U pdns -d pdns`
```
create schema pdns;
grant all on schema pdns to pdns;

alter database pdns set search_path to pdns;
CREATE TABLE domains (
  id                    SERIAL PRIMARY KEY,
  name                  VARCHAR(255) NOT NULL,
  master                VARCHAR(128) DEFAULT NULL,
  last_check            INT DEFAULT NULL,
  type                  VARCHAR(6) NOT NULL,
  notified_serial       INT DEFAULT NULL,
  account               VARCHAR(40) DEFAULT NULL,
  CONSTRAINT c_lowercase_name CHECK (((name)::TEXT = LOWER((name)::TEXT)))
);

CREATE UNIQUE INDEX name_index ON domains(name);


CREATE TABLE records (
  id                    BIGSERIAL PRIMARY KEY,
  domain_id             INT DEFAULT NULL,
  name                  VARCHAR(255) DEFAULT NULL,
  type                  VARCHAR(10) DEFAULT NULL,
  content               VARCHAR(65535) DEFAULT NULL,
  ttl                   INT DEFAULT NULL,
  prio                  INT DEFAULT NULL,
  disabled              BOOL DEFAULT 'f',
  ordername             VARCHAR(255),
  auth                  BOOL DEFAULT 't',
  CONSTRAINT domain_exists
  FOREIGN KEY(domain_id) REFERENCES domains(id)
  ON DELETE CASCADE,
  CONSTRAINT c_lowercase_name CHECK (((name)::TEXT = LOWER((name)::TEXT)))
);

CREATE INDEX rec_name_index ON records(name);
CREATE INDEX nametype_index ON records(name,type);
CREATE INDEX domain_id ON records(domain_id);
CREATE INDEX recordorder ON records (domain_id, ordername text_pattern_ops);


CREATE TABLE supermasters (
  ip                    INET NOT NULL,
  nameserver            VARCHAR(255) NOT NULL,
  account               VARCHAR(40) NOT NULL,
  PRIMARY KEY(ip, nameserver)
);


CREATE TABLE comments (
  id                    SERIAL PRIMARY KEY,
  domain_id             INT NOT NULL,
  name                  VARCHAR(255) NOT NULL,
  type                  VARCHAR(10) NOT NULL,
  modified_at           INT NOT NULL,
  account               VARCHAR(40) DEFAULT NULL,
  comment               VARCHAR(65535) NOT NULL,
  CONSTRAINT domain_exists
  FOREIGN KEY(domain_id) REFERENCES domains(id)
  ON DELETE CASCADE,
  CONSTRAINT c_lowercase_name CHECK (((name)::TEXT = LOWER((name)::TEXT)))
);

CREATE INDEX comments_domain_id_idx ON comments (domain_id);
CREATE INDEX comments_name_type_idx ON comments (name, type);
CREATE INDEX comments_order_idx ON comments (domain_id, modified_at);


CREATE TABLE domainmetadata (
  id                    SERIAL PRIMARY KEY,
  domain_id             INT REFERENCES domains(id) ON DELETE CASCADE,
  kind                  VARCHAR(32),
  content               TEXT
);

CREATE INDEX domainidmetaindex ON domainmetadata(domain_id);


CREATE TABLE cryptokeys (
  id                    SERIAL PRIMARY KEY,
  domain_id             INT REFERENCES domains(id) ON DELETE CASCADE,
  flags                 INT NOT NULL,
  active                BOOL,
  content               TEXT
);

CREATE INDEX domainidindex ON cryptokeys(domain_id);


CREATE TABLE tsigkeys (
  id                    SERIAL PRIMARY KEY,
  name                  VARCHAR(255),
  algorithm             VARCHAR(50),
  secret                VARCHAR(255),
  CONSTRAINT c_lowercase_name CHECK (((name)::TEXT = LOWER((name)::TEXT)))
);

CREATE UNIQUE INDEX namealgoindex ON tsigkeys(name, algorithm);
```
#### fill with entries ####
get uBlockOrigin.zones file  
get db.block and change the domain  
put it in `/etc/named/` or `sed -i 's/\/etc\/named\/db.block/\/root\/db.block/' uBlockOrigin.zones` to change the dir to /root  

`zone2sql --named-conf=/root/uBlockOrigin.zones --transactions -gpgsql | psql -d pdns -U pdns -q >psql.log 2>&1 &`
