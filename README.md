# Adblock

### Bind9 based adblocker

## What's Up

Taking authority for a zone and pointing to an empty zonefile

## Bind Config

* The script assumes systemd and named.service (may be bind9.service)

* You need ```include "/etc/named/blacklist.zones";``` in your named.conf

* And a zonefile in ```/etc/named/db.block``` like:

```
;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     adblock.test.netz. mail.test.netz. (
3        ; Serial
604800   ; Refresh
86400    ; Retry
2419200  ; Expire
604800 ) ; Negative Cache TTL
;
NS      adblock.test.netz.
TXT     Domain blocked.
```

## Usage

Example:

```./adblock add example.com``` to add empty zone for example.com

Help:

```
Usage:
adblock usage
adblock add example.com
adblock del example.com
adblock list domain.txt
```
